﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationLibrary1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

     namespace Calculated
    {
         public abstract class Calc
        {
            //int Sum(int a, int b);
            // int Res(int a, int b);
            protected abstract string toSysA(int i);
            protected abstract int toInt(string a);
            public string culc(string a, string b, int opr)
            {
                int ai, bi;
                ai = toInt(a);
                bi = toInt(b);
                if (opr == 1)
                {
                    return toSysA(ai + bi);
                }
                if (opr == 2)
                {
                    return toSysA(ai - bi);
                }
                throw new Exception("uncnow opr,opr mast equal \"+\" or \"-\" ");
            }
        }

        public class BinCulc : Calc
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 2);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 2);
            }
        }
        public class DecCulc : Calc
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 10);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 10);
            }
        }
        public class DexCulc : Calc
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 16);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 16);
            }
        }


    }

}
